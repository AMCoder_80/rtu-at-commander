#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "modem.h"

// Diag level can be 0 - 1 - 2 - 3
#define DIAG_LEVEL 0


// ===============================================  Opening serial port function
/*
INPUTS:
    Device name of addredd
    Desired baud rate

OUTPUT:
    < 0 => in case of failure
    >= 0 => in case of success
*/
int init_modem(const char * device, uint32_t baud_rate)
{
    int fd = open(device, O_RDWR | O_NOCTTY);

    if (fd == -1)
        {
            if (DIAG_LEVEL > 0) printf("Failed to open serial port\n");
            return -1;
        }

    int result = tcflush(fd, TCIOFLUSH);
    if (result)
        {
            if (DIAG_LEVEL > 0) printf("Failed to flush serial buffer\n");
        }

    struct termios options;
    result = tcgetattr(fd, &options);
    if (result)
        {
            if (DIAG_LEVEL > 1) printf("tcgetattr failed");
            close(fd);
            return -1;
        }

    options.c_iflag &= ~(INLCR | IGNCR | ICRNL | IXON | IXOFF);
    options.c_oflag &= ~(ONLCR | OCRNL);
    options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

    options.c_cc[VTIME] = 1;
    options.c_cc[VMIN] = 0;

    switch (baud_rate)
    {
        case 4800:   cfsetospeed(&options, B4800);   break;
        case 9600:   cfsetospeed(&options, B9600);   break;
        case 19200:  cfsetospeed(&options, B19200);  break;
        case 38400:  cfsetospeed(&options, B38400);  break;
        case 115200: cfsetospeed(&options, B115200); break;
        default:
            fprintf(stderr, "warning: baud rate %u is not supported, using 9600.\n",
            baud_rate);
            cfsetospeed(&options, B9600);
            break;
    }
    cfsetispeed(&options, cfgetospeed(&options));
    result = tcsetattr(fd, TCSANOW, &options);
    if (result)
    {
        if (DIAG_LEVEL > 0) printf("tcsetattr failed");
        close(fd);
        return -1;
    }

    return fd;
}

// =============================================== Serial buffer flush function
/*
Makes the serial buffer empty
*/
int serial_flush(int port)
{
    return tcflush(port, TCIOFLUSH);
}

// ===============================================  Writing to port function
/*
INPUTS:
    opened serial port
    content of buffer
    size of buffer

OUTPUTS:
    < 0 => in case of failure
    >= 0 => in case of success
*/
int write_port(int fd, char * buffer, size_t size)
{
    if (fd < 0)
    {
        return -1;
    }

    ssize_t result = write(fd, buffer, size);

    if (result != (ssize_t)size)
    {
        if (DIAG_LEVEL > 0) printf("failed to write to port");
        return -1;
    }
    return 0;
}


// ===============================================  Read from port function
/*
INPUTS:
    opened serial port
    content of buffer
    size of buffer

OUTPUTS:
    < 0 => in case of failure
    >= 0 => in case of success
*/
ssize_t read_port(int fd, char * buffer, size_t size)
{
    if (fd < 0)
    {
        return -1;
    }

    size_t received = 0;
    while (received < size)
    {
        ssize_t r = read(fd, buffer + received, size - received);
        if (r < 0)
        {
        if (DIAG_LEVEL > 0) printf("failed to read from port");
        return -1;
        }
        if (r == 0)
        {
        break;
        }
        received += r;
    }
    return received;
}


// ===============================================  Read a special string function
/*
This function will take 1 sec for each number of timeOut

INPUTS:
    opened serial port
    the pattern need to be searched
    maximum timeOut

OUTPUTS:
    0 => in case of failure
    1 => in case of success
*/
int read_comm(int port, char pattern[], int timeOut)
{
    char resp[255];

    while (strstr(resp, pattern) == NULL && timeOut){
            read_port(port, resp, 120);
            timeOut--;
            sleep(1);
    }

    if (!timeOut){
        return 0;
    }
    return 1;
}


// ===============================================  Delivery report function
/*
Checks weather or not the message has been recieved to the reciever

INPUTS:
    opened serial port
    maximum timeOut

OUTPUTS:
    0 => in case of failure
    1 => in case of success
*/
int delivered(int port, int timeOut)
{

    char content[255];

    // +CDS: Will be appeared with some detail in case of message delivery
    while (strstr(content, "+CDS:") == NULL && timeOut){
            read_port(port, content, 120);
            timeOut--;
            sleep(1);
    }
    
    if (!timeOut){
        if (DIAG_LEVEL > 1) printf("Failed to deliver!\n");
        return 0;
    }

    int camma=0;
    int is_recieved=0;
    char id[50];
    char number[50];

    memset(id, 0, sizeof(id));
    memset(number, 0, sizeof(number));

    // Parse the whole command to extract id, number and status of the message
    for (int j=0; j<=strlen(content); j++){

            if (content[j] == ',')
            {
                camma++;
            }
            
            if (camma == 1 && isdigit(content[j]))
            {
                // append to the string
                sprintf(id, "%s%c", id, content[j]);
            }
            else if (camma == 2 && isdigit(content[j]))
            {
                sprintf(number, "%s%c", number, content[j]);
            }
            else if (camma == 8 && content[j] == '0')
            {
                is_recieved = 1;
            }
    }

    if (is_recieved)
    {
        if (DIAG_LEVEL > 2) printf("SMS ID: %s \n", id);
        if (DIAG_LEVEL > 2) printf("Phone number: %s \n", number);
        if (DIAG_LEVEL > 2) printf("Status: Received \n");
        return 1;
    }

    return 0;
}


// ===============================================  Calling number function
/*
INPUTS:
    opened serial port
    destination mobile number
    maximum timeOut

OUTPUTS:
    0 => in case of failure
    1 => in case of success
*/
ssize_t call_number(int port, char number[], int timeOut)
{
    char req[255];

    // Required AT command for making a voice call
    sprintf(req, "ATD%s;\r\n", number);
    write_port(port, req, strlen(req));

    if (read_comm(port, "OK", timeOut)){
        int test = read_comm(port, "NO", timeOut);
        if(test){
            if (DIAG_LEVEL > 1) printf("Failed to making the call\n");
        }
        else{
            if (DIAG_LEVEL > 2) printf("Call was dialed successfully\n");
        }
    }
    else{
        if (DIAG_LEVEL > 1) printf("No response received...!\n");
        return 0;
    }
    return 1;
}


// ===============================================  Sending single page sms function
/*
Send sms with the length of less than 120 char

INPUTS:
    opened serial port
    main text to be sent
    destination mobile number
    maximum timeOut

OUTPUTS:
    0 => in case of failure
    1 => in case of success
*/
ssize_t single_page_sms(int port, char text[], char number[], int timeOut)
{
    char req[255];
    memset(req, 0, sizeof(req));

    // Requried AT command to switch to text mode
    write_port(port, "AT+CMGF=1\r", strlen("AT+CMGF=1\r"));
    if(!read_comm(port, "OK", timeOut)){
        if (DIAG_LEVEL > 1) printf("No response...!\n");
        return 0;
    }

    // Requried AT command to send single page sms
    sprintf(req, "AT+CMGS=\"%s\"\r", number);
    write_port(port, req, strlen(req));
    if(!read_comm(port, ">", timeOut)){
        if (DIAG_LEVEL > 1) printf("No response...!\n");
        return 0;
    }

    write_port(port, text, strlen(text));
    sprintf(req, "%c", (char)26);
    write_port(port, req, strlen(req));
    if(!read_comm(port, "OK", timeOut)){
        if (DIAG_LEVEL > 1) printf("No response...!\n");
        return 0;
    }

    if(delivered(port, timeOut)){
        if (DIAG_LEVEL > 1) printf("Message delivered\n");
    }
    else{
        if (DIAG_LEVEL > 1) printf("Message did not deliver\n");
    }

    return 1;
}


// ===============================================  Send multiple page sms function
/*
Send sms with the length of less than 840 char

INPUTS:
    opened serial port
    main text
    destination mobile number
    maximum timeOut

OUTPUTS:
    0 => in case of failre
    1 => in case of success
*/
ssize_t multi_page_sms(int port, char text[], char number[], int timeOut)
{
    char req[255];
    memset(req, 0, sizeof(req));
    char splited_text[300];
    int char_per_page = 120;
    int rand_id = rand() % 155;
    int segments = (strlen(text) / char_per_page);

    if (strlen(text) % char_per_page){
        segments++;
    }

    for(int i=0; i<segments; i++){
        sleep(1);

        // Required AT command to send multipage sms
        sprintf(req, "AT+QCMGS=\"%s\",%i,%i,%i\r", number, rand_id, i+1, segments);
        write_port(port, req, strlen(req));
        if(!read_comm(port, ">", timeOut)){
        if (DIAG_LEVEL > 1) printf("No response for >...!\n");
        return 0;
        }

        memset(splited_text, 0, sizeof(splited_text));
        for (int b = (i * char_per_page); b<(char_per_page+(i*char_per_page)); b++){
                if(b < strlen(text)){
                sprintf(splited_text, "%s%c", splited_text, text[b]);
                }
            }

        write_port(port, splited_text, strlen(splited_text));
        sleep(0.2);

        sprintf(req, "%c\n\r", (char)26);
        write_port(port, req, strlen(req));

        if(!read_comm(port, "OK", timeOut)){
            if (DIAG_LEVEL > 1) printf("No response... for ok!\n");
            return 0;
        }
    }

        if(delivered(port, timeOut)){
            if (DIAG_LEVEL > 1) printf("Message delivered\n");
        }
        else{
            if (DIAG_LEVEL > 1) printf("Message did not deliver\n");
        }

    return 1;
}


// =============================================== Sms load balancer function
/*
Decides which sms should be sent by which function

INPUTS:
    opened serial port
    main text
    destination mobile number
    maximum timeOut

OUTPUT:
    NO RETURN VALUE
*/
void send_sms(int port, char text[], char number[], int timeOut)
{
    // Initial commands are required to send sms successfully
    initial_commands(port);

    if (strlen(text) > 120){
        if (DIAG_LEVEL > 2) printf("Sending sms in multi plage mode \n");
        multi_page_sms(port, text, number, timeOut);
    }
    else{
        if (DIAG_LEVEL > 2) printf("Sending sms in single page mode \n");
        single_page_sms(port, text, number, timeOut);
    }
}


// ===============================================  Modem initialization function
/*
Initial commands for other functions

INPUTS:
    opened serial port

OUTPUTS:
    NO RETURN VALUE
*/
void initial_commands(int port)
{
    char comm[50];
    // Enabling deliver report functionality AT commands
    sprintf(comm, "AT+CSMP=49,167,0,0\r\n");
    write_port(port, comm, strlen(comm));
    sleep(1);
    sprintf(comm, "AT+CNMI=2,1,0,1,0\r\n");
    write_port(port, comm, strlen(comm));
    sleep(1);
    // Set sms to text mode
    sprintf(comm, "AT+CMGF=1\r\n");
    write_port(port, comm, strlen(comm));
}


// ===============================================  Dynamic report function
/*
Get dynamic attributes such as signal quality level

INTPUTS:
    opened serial port

OUTPUTS:
    NO RETURN VALUE
*/
void dynamic_report(int port)
{

    // Required AT command to get signal quality
    char comm[] = "AT+CSQ\r\n";
    char resp[255];
    char text[255];
    int sign = 0;

    memset(text,0,sizeof(text));
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if (resp[j] == ':')
        {
            sign++;
        }
        else if (resp[j] == ',')
        {
            break;
        }
        if(sign == 1 && isdigit(resp[j])){
            strncat(text, &resp[j], 1);
        }
    }
    if (DIAG_LEVEL > 2) printf("Signal value is: %i\n", atoi(text));
}



// ===============================================  Static report function
/*
Get static attributes such as IMEI or manufacturer code

INPUTS:
    opened serial port

OUTPUT:
    NO RETURN VALUE
*/
void static_report(int port)
{
    char comm[255];
    char resp[255];
    char text[255];
    int new_line = 0;
    int quote = 0;
    int limit = 0;

/// =============================== Operator checking
    memset(comm,0,sizeof(comm));
    memset(resp,0,sizeof(resp));
    memset(text,0,sizeof(text));
    
    // Required AT command for getting operator's name
    sprintf(comm, "AT+COPS?\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){

        if(quote == 1 && limit < 8){
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '"'){
            quote++;
        }
    }
    if (DIAG_LEVEL > 2) printf("\nRegistered operator is: %s\n", text);

/// ================================ Manufacturer Name
    memset(comm,0,sizeof(comm));
    memset(resp,0,sizeof(resp));
    memset(text,0,sizeof(text));

    new_line = 0;
    limit = 0;

    // Required AT command for getting manufacturer name
    sprintf(comm, "AT+GMI\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(new_line == 1 && limit < 8) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    if (DIAG_LEVEL > 2) printf("Manufacturer Name is: %s\n", text);

/// ================================= Brand Name
    memset(comm,0,sizeof(comm));
    memset(resp,0,sizeof(resp));
    memset(text,0,sizeof(text));

    new_line = 0;
    limit = 0;

    // Required AT command for getting brand name
    sprintf(comm, "AT+GMM\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){

        if(new_line == 1 && limit < 4) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    if (DIAG_LEVEL > 2) printf("Brand name is: %s\n", text);
/// ================================== Version
    memset(comm,0,sizeof(comm));
    memset(resp,0,sizeof(resp));
    memset(text,0,sizeof(text));

    new_line = 0;
    limit = 0;

    // Required AT command for getting module's version
    sprintf(comm, "AT+GMR\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(new_line == 1 && limit < 19) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    if (DIAG_LEVEL > 2) printf("Version is: %s\n", text);
/// ================================== IMEI Code
    memset(comm,0,sizeof(comm));
    memset(resp,0,sizeof(resp));
    memset(text,0,sizeof(text));

    new_line = 0;
    limit = 0;

    // Required AT command for getting module's IMEI code
    sprintf(comm, "AT+GSN\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(new_line == 1 && limit < 16) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    if (DIAG_LEVEL > 2) printf("IMEI Code is: %s\n\n", text);
}


// ============================================================  Persian SMS
/*
Send sms in persian with the limitation of 70 chars per sms

INPUTS:
    opened serial port
    main message in persian
    destination mobile number
    maximum timeOut

OUTPUTS:
    0 => in case of failure
    1 => in case of success
*/
int persian_sms(int port, char message[], char phone_number[], int timeOut)
{

    // Setting up a lookup table for relation between codes and chars
    char *letters[] = {" ", "ض", "ص", "ث", "ق", "ف", "غ", "ع", "ه", "خ", "ح", "ج", "ش", "س", "ی", "ب", "ل", "ا", "ت", "ن", "م", "ک", "ظ", "ط", "ز", "ر", "ذ", "د", "و", "گ", "چ", "پ", "ژ"};
    const char *indexes[] = {"0636", "0635", "062B", "0642", "0641", "063A", "0639", "0647", "062E", "062D", "062C", "0634", "0633", "06CC", "0628", "0644", "0627", "062A", "0646", "0645", "06A9", "0638", "0637", "0632", "0631", "0630", "062F", "0648", "06AF", "0686", "067E", "0698", "0020"};
    
    // Calculate the length of lookup table
    const int len = (sizeof(indexes) / sizeof(char *));
    int temp;
    char decodes[400];
    char char_name[3];
    char final_string[1000];

    memset(final_string, 0, sizeof(final_string));
    memset(decodes, 0, sizeof(decodes));

    strcat(final_string, "079189290000900231000B81");

    // Swich to PDU mode
    write_port(port, "AT+CMGF=0\r\n", strlen("AT+CMGF=0\r\n"));
    sleep(1);


    if (strlen(message) > 70)
    {
        if (DIAG_LEVEL > 1) printf("Your message is too long.\nThe maximum length should be 70 chars\n");
        return 0;
    }

    strcat(phone_number, "F");


    for (int i=0; i<strlen(phone_number); i+=2){
        char_name[0] = phone_number[i+1];
        char_name[1] = phone_number[i];
        char_name[2] = '\0';
        strcat(final_string, char_name);
    }
    if (DIAG_LEVEL > 2) printf("The phone number added: %s \n", final_string);
    strcat(final_string, "0008FF");


    int i=0;
    while (i<strlen(message)){
        if (message[i] == ' '){
            strcat(decodes, indexes[len-1]);
            i++;
        }
        else{
            char_name[0] = message[i];
            
            char_name[1] = message[i+1];
            char_name[2] = '\0';
            for (int j=0; j<len; j++){
                if(!strcmp(letters[j], char_name)){
                    strcat(decodes, indexes[j-1]);
                }
            }
            i+=2;
    }
    }

    if (DIAG_LEVEL > 2) printf("This is decoded: %s \n", decodes);

    char size_of_num[5];
    char comm[700];
    temp = strlen(decodes) / 2;
    sprintf(size_of_num, "%x", temp);

    if (strlen(size_of_num) == 1){
        strcat(final_string, "0");
    }
    strcat(final_string, size_of_num);
    if (DIAG_LEVEL > 2) printf("Before con: %s \n", final_string);
    strcat(final_string, decodes);
    if (DIAG_LEVEL > 2) printf("After con: %s \n", final_string);


    if (DIAG_LEVEL > 2) printf("The final string is: %s\n", final_string);

    temp = strlen(final_string) - 16;
    temp /= 2;
    if (DIAG_LEVEL > 2) printf("The total length should be: %i \n", temp);

    // Start sending sms with proper AT command
    sprintf(comm, "AT+CMGS=%i\r", temp);
    write_port(port, comm, strlen(comm));
    read_comm(port, ">", timeOut);

    write_port(port, final_string, strlen(final_string));
    sprintf(comm, "%c", (char)26);
    write_port(port, comm, strlen(comm));

    read_comm(port, "OK", timeOut);

    sleep(1);
    // Swich back to text mode
    write_port(port, "AT+CMGF=1\r\n", strlen("AT+CMGF=1\r\n"));
    sleep(1);

    return 1;
}


// =================================================================  Read all the messages from the memory function
/*
Extracts Phone number, Date and the main text from sms body
then store them in a text file

INPUTS:
    message text to be parsed

OUTPUTS:
    NO RETURN VALUE
*/
void founder(char* resp)
{ 
    int newlines = 0;
    int cammas = 0;
    char per;
    
    char text[255];
    char number[50];
    char date[50];
    char content[500];
    memset(text, 0, sizeof(text));
    memset(number, 0, sizeof(number));
    memset(date, 0, sizeof(date));
    memset(content, 0, sizeof(content));
    
    for (int i = 0; i < strlen(resp); i++)
    {
        // Gettting the char at this memory address
        per = *(resp + i);
        
       if (per == '\n')
       {
           newlines++;
       }
       else if (per == ',')
       {
           cammas++;
       }
       else if (per != '"')
       {
           
           if (newlines == 1 && cammas==1)
           {
               sprintf(number, "%s%c", number, per);
           }
           else if (newlines == 1 && cammas == 3)
           {
               sprintf(date, "%s%c", date, per);
           }
           else if (newlines == 2 && cammas == 4)
           {
               sprintf(text, "%s%c", text, per);
           }
       }
        
    }

    // Store extracted data into a text file
    if (DIAG_LEVEL > 2) printf("Number is: %s\n", number);
    if (DIAG_LEVEL > 2) printf("Date is: %s\n", date);
    if (DIAG_LEVEL > 2) printf("Text is: %s\n\n", text);
    FILE *my_file = fopen("./content.txt", "a");
    sprintf(content, "Sender number: %s\n", number);
    fprintf(my_file, content);
    sprintf(content, "Content: %s\n", text);
    fprintf(my_file, content);
    sprintf(content, "Date: %s\n\n", date);
    fprintf(my_file, content);
    fclose(my_file);
    
}


// =============================================== Read sms
/*
Checks weather of not a new sms has been recieved. 
then store it with the help of founder function

INPUTS:
    opened serial port

OUTPUTS:
    NO RETURN VALUE
*/
void read_sms(int port)
{
    int i = 0;
    char response[500];
    char req[200];
    int len = 0;
    memset(response, 0, sizeof(response));
    memset(req, 0, sizeof(req));

    while (1)
    {
        // Required AT command to read a specific message from module's memory
        sprintf(req, "AT+CMGR=%i\r\n", i);
        write_port(port, req, strlen(req));
        sleep(0.3);
        len = read_port(port, response, sizeof(response));

        if(len < 60)
        {
            break;
        }

        founder(response);  

        // Delete read sms with proper AT command
        sprintf(req, "AT+CMGD=%i\r\n", i);
        write_port(port, req, strlen(req));
        sleep(0.6);

        i++;
        
    }
}


// =============================================== Async Function
/*
Get called per each command
this function operates on another thread

INPUTS:
    command struct

OUTPUTS:
    NO RETURN VALUE
*/
void* command_reader(void* arg)
{
    command *data = (command *)arg;

    if (!strcmp(data->type, "call"))
    {
        // printf("Calling to %s \n", data->number);
        call_number(data->port, data->number, 20);
    }
    else if (!strcmp(data->type, "sms"))
    {
        // printf("Sending %s to %s \n", data->text, data->number);
        send_sms(data->port, data->text, data->number, 20);
    }
    else if (!strcmp(data->type, "persian"))
    {
        persian_sms(data->port, data->text, data->number, 20);
    }

    read_sms(data->port);
}
