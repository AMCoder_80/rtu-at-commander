#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "modem.h"


int main(void){

    const char * device = "/dev/AMS_EC25_AT";
    uint32_t baud_rate = 115200;

    int port = init_modem(device, baud_rate);

    // pthread_create(&buff_reader, NULL, cmd_reader, &port);


    if(port < 0){
        return 1;
    }

    initial_commands(port);
    sleep(2);
    while (1){
        serial_flush(port);

        char cmd[255];

        printf("Enter a cmd to be executed: ");
        scanf("%s", cmd);

        if (!strcmp(cmd, "stop")) {
            break;
        }
        else if (!strcmp(cmd, "call")){
            // call_number(port, 10);
        }
        else if(!strcmp(cmd, "dynamic")){
            dynamic_report(port);
        }
        else if(!strcmp(cmd, "static")){
            static_report(port);
        }
        else if(!strcmp(cmd, "sms")){
            // send_sms(port, 50);
        }
        else if(!strcmp(cmd, "per")){
            // persian_sms(port, 50);
        }
        else if(!strcmp(cmd, "read")){
            // read_sms(port);
        }
        else if(!strcmp(cmd, "new")){
            char type[20];
            printf("Enter the type of action: ");
            scanf("%s", type);
            char number[20];
            printf("Enter the phone number: ");
            scanf("%s", number);
            char text[50];
            printf("Enter the desired text: ");
            scanf("%s", text);
            command data = {port, type, number, text};
            pthread_t buff_reader;
            pthread_create(&buff_reader, NULL, command_reader, &data);
            // pthread_join(buff_reader, NULL);
        }
        else{
            printf("Not supported cmd :( \n");
        }
    }
    printf("\n ===> The program has been terminated <=== \n");
    close(port);
    return 0;
}
