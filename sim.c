#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <math.h>


typedef struct command
{
  char *type;
  char *number;
  char *text;
}command;

const int BUFFER_SIZE = 10;
command* buffer[3 * BUFFER_SIZE] = {NULL};


// ===================================================================  Opening serial port function
int init_modem(const char * device, uint32_t baud_rate)
{
    int fd = open(device, O_RDWR | O_NOCTTY);

    if (fd == -1)
        {
            printf("Failed to open serial port\n");
            return -1;
        }

    int result = tcflush(fd, TCIOFLUSH);
    if (result)
        {
            printf("Failed to flush serial buffer\n");
        }

    struct termios options;
    result = tcgetattr(fd, &options);
    if (result)
        {
            printf("tcgetattr failed");
            close(fd);
            return -1;
        }

    options.c_iflag &= ~(INLCR | IGNCR | ICRNL | IXON | IXOFF);
    options.c_oflag &= ~(ONLCR | OCRNL);
    options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

    options.c_cc[VTIME] = 1;
    options.c_cc[VMIN] = 0;

    switch (baud_rate)
    {
        case 4800:   cfsetospeed(&options, B4800);   break;
        case 9600:   cfsetospeed(&options, B9600);   break;
        case 19200:  cfsetospeed(&options, B19200);  break;
        case 38400:  cfsetospeed(&options, B38400);  break;
        case 115200: cfsetospeed(&options, B115200); break;
        default:
            fprintf(stderr, "warning: baud rate %u is not supported, using 9600.\n",
            baud_rate);
            cfsetospeed(&options, B9600);
            break;
    }
    cfsetispeed(&options, cfgetospeed(&options));
    result = tcsetattr(fd, TCSANOW, &options);
    if (result)
    {
        printf("tcsetattr failed");
        close(fd);
        return -1;
    }
    return fd;
}


// ===================================================================  Writing to port function
int write_port(int fd, uint8_t * buffer, size_t size)
{
  ssize_t result = write(fd, buffer, size);

  if (result != (ssize_t)size)
  {
    printf("failed to write to port");
    return -1;
  }
  return 0;
}


// ===================================================================  Read from port function
ssize_t read_port(int fd, uint8_t * buffer, size_t size)
{
  size_t received = 0;
  while (received < size)
  {
    ssize_t r = read(fd, buffer + received, size - received);
    if (r < 0)
    {
      printf("failed to read from port");
      return -1;
    }
    if (r == 0)
    {
      break;
    }
    received += r;
  }
  return received;
}


void founder(char* resp)
{
    
    int newlines = 0;
    int cammas = 0;
    int len = strlen(resp);
    char per;
    
    char text[255];
    char number[50];
    char date[50];
    char content[500];
    memset(text, 0, sizeof(text));
    memset(number, 0, sizeof(number));
    memset(date, 0, sizeof(date));
    memset(content, 0, sizeof(content));


    printf("\nInside this function =====> \n");
    
    for (int i = 0; i < len; i++)
    {
        per = *(resp + i);
        
       if (per == '\n')
       {
           newlines++;
       }
       else if (per == ',')
       {
           cammas++;
       }
       else if (per != '"')
       {
           
           if (newlines == 1 && cammas==1)
           {
               sprintf(number, "%s%c", number, per);
           }
           else if (newlines == 1 && cammas == 3)
           {
               sprintf(date, "%s%c", date, per);
           }
           else if (newlines == 2 && cammas == 4)
           {
               sprintf(text, "%s%c", text, per);
           }
       }
        
    }
    printf("Number is: %s\n", number);
    printf("Date is: %s\n", date);
    printf("Text is: %s\n\n", text);
    FILE *my_file = fopen("./content.txt", "a");
    sprintf(content, "Sender number: %s\n", number);
    fprintf(my_file, content);
    sprintf(content, "Content: %s\n", text);
    fprintf(my_file, content);
    sprintf(content, "Date: %s\n\n", date);
    fprintf(my_file, content);
    fclose(my_file);
    
}


void read_me(int port)
{
    int i = 0;
    char response[500];
    char req[200];
    int len = 0;
    memset(response, 0, sizeof(response));
    memset(req, 0, sizeof(req));

    sprintf(req, "AT+CMGR=%i\r\n", i);
    write_port(port, req, strlen(req));
    sleep(0.3);

    while (1)
    {
        sprintf(req, "AT+CMGR=%i\r\n", i);
        write_port(port, req, strlen(req));
        sleep(0.3);
        len = read_port(port, response, sizeof(response));

        if(len < 30)
        {
            break;
        }

        founder(response);

        i++;
        
    }
}

// ===================================================================  Read a special string function
int read_comm(int port, char pattern[], int timeOut){

    int len = 0;
    char resp[120];

    while (strstr(resp, pattern) == NULL && timeOut){
            read_port(port, resp, 120);
            timeOut--;
            sleep(1);
    }

    if (!timeOut){
        return 0;
    }
    return 1;
}


// ===================================================================  Delivery report function
int delivered(int port){
    int i=0;
    int len = 0;
    char content[120];
    int timeOut = 20;

    while (strstr(content, "+CDS:") == NULL && timeOut){
            read_port(port, content, 120);
            timeOut--;
            sleep(1);
    }
    
    if (!timeOut){
        printf("Failed to deliver!\n");
        return 0;
    }
   

    for (int j=0; j<=strlen(content); j++){
            if (i >= 8){
                if (content[j] == '0'){
                    return 1;
                }
            }
            else if (content[j] == ','){
            i++;
        }
    }
    return 0;
}

// ===================================================================  Calling number function
ssize_t call_number(int port, int timeOut){
    
    char req[50];
    char number[50];
    int len = 0;

    printf("Phone Number: ");
    scanf("%s", &number);

    sprintf(req, "ATD%s;\r\n", number);
    write_port(port, req, strlen(req));

    char resp[50];
    if (read_comm(port, "OK", timeOut)){
        int test = read_comm(port, "NO", timeOut);
        if(test){
            printf("Failed to making the call\n");
        }
        else{
            printf("Call was dialed successfully\n");
        }
    }
    else{
        printf("No response received...!\n");
        return 0;
    }
    return 1;
}

// ===================================================================  Sending single page sms function
ssize_t single_page_sms(int port, char text[], char number[], int timeOut){

    char req[255];
    char resp[255];
    int len = 0;

    write_port(port, "AT+CMGF=1\r", strlen("AT+CMGF=1\r"));
    // sleep(0.3);
    if(!read_comm(port, "OK", timeOut)){
        printf("No response...!\n");
        return 0;
    }
    sprintf(req, "AT+CMGS=\"%s\"\r", number);
    write_port(port, req, strlen(req));
    
    if(!read_comm(port, ">", timeOut)){
        printf("No response...!\n");
        return 0;
    }

    write_port(port, text, strlen(text));
    sprintf(req, "%c", (char)26);
    write_port(port, req, strlen(req));
    
    if(!read_comm(port, "OK", timeOut)){
        printf("No response...!\n");
        return 0;
    }

    if(delivered(port)){
        printf("Message delivered\n");
    }
    else{
        printf("Message did not deliver\n");
    }

    return 1;
}


// ===================================================================  Send multiple page sms function
ssize_t multi_page_sms(int port, char text[], char number[], int timeOut){

    char req[255];
    char resp[50];
    char splited_text[255];
    int char_per_page = 100;
    int rand_id = rand() % 155;
    int segments = (strlen(text) / char_per_page);

    if (strlen(text) / char_per_page){
        segments++;
    }

    for(int i=0; i<segments; i++){
        sprintf(req, "AT+QCMGS=\"%s\",%i,%i,%i\r", number, rand_id, i+1, segments);
        write_port(port, req, strlen(req));
        
        if(!read_comm(port, ">", timeOut)){
        printf("No response...!\n");
        return 0;
        }

        memset(splited_text, 0, strlen(splited_text));
        for (int b = (i * char_per_page); b<(char_per_page+(i*char_per_page)); b++){
                if(b < strlen(text)){
                strncat(splited_text, &text[b], 1);
                }
            }
        write_port(port, splited_text, strlen(splited_text));
        sleep(0.2);

        sprintf(req, "%c", (char)26);
        write_port(port, req, strlen(req));

        if(!read_comm(port, "OK", timeOut)){
            printf("No response...!\n");
            return 0;
        }
        printf("another here\n");
        sleep(1);
        }

        if(delivered(port)){
            printf("Message delivered\n");
        }
        else{
            printf("Message did not deliver\n");
        }
    
    return 1;
}


// ===================================================================  Modem initialization function
void initial_commands(int port){

    char comm[50];
    sprintf(comm, "AT+CSMP=49,167,0,0\r\n");
    write_port(port, comm, strlen(comm));
    sleep(0.5);
    sprintf(comm, "AT+CNMI=2,1,0,1,0\r\n");
    write_port(port, comm, strlen(comm));
    sleep(0.5);
    sprintf(comm, "AT+CMGF=1\r\n");
    write_port(port, comm, strlen(comm));
}


// ===================================================================  Dynamic report function
void dynamic_report(int port){

    char comm[] = "AT+CSQ\r\n";
    char resp[100];
    char text[255];
    int sign = 0;
    int limit = 0;

    memset(text,0,strlen(text));
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(sign == 1 && limit < 6){
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == ':'){
            sign++;
        }
    }
    printf("Signal value is: %s\n", text);

}


// ===================================================================  Reading sms and store them function
void read_sms(int port){

    // prepare(port);

    char command[] = "AT+CMGL=\"ALL\"\r\n";
    char resp[256];
    int len = 0;

    write_port(port, command, strlen(command));
    sleep(0.5);
    len = read_port(port, resp, sizeof(resp));

    printf("This is the len %i and message: %s \n", len, resp);

    if (len > 30){

    char* starter = strstr(resp, "+CMGL:");
    int ji=0;
    
    int new_line = 0;
    int camma = 0; /// Compile the whole lib again tomorrow
    char date[255];
    char number[20];
    char text[400];
    char content[700];
    memset(date,0,sizeof(date));
    memset(number,0,sizeof(number));
    memset(text,0,sizeof(text));
    memset(content,0,sizeof(content));
    
    while (*(starter + ji) != '\0'){
        ji++;
    
        for (int i=0; i<strlen(resp); i++){
            if (resp[i] == '\n'){
                new_line++;
                }
            else if(resp[i] == ','){
                camma++;
            }
    
            if(new_line == 1 && camma == 2){
    
                if(resp[i] != '"' && resp[i] != ','){
                    strncat(number, &resp[i], 1);
                }
            }
    
            else if(camma >= 4 && new_line == 1){
    
                if(resp[i] != '"' && resp[i] != ','){
                    strncat(date, &resp[i], 1);
                }
            }
            else if(camma == 5 && new_line == 2){
                if(resp[i] != '\n'){
                    strncat(text, &resp[i], 1);
                }
            }
        }
    }

        printf("\nNumber is: %s\n", number);
        printf("Date is: %s\n", date);
        printf("Text is: %s\n\n", text);
        FILE *my_file = fopen("./content.txt", "a");
        sprintf(content, "Sender number: %s\n", number);
        fprintf(my_file, content);
        sprintf(content, "Content: %s\n", text);
        fprintf(my_file, content);
        sprintf(content, "Date: %s\n\n", date);
        fprintf(my_file, content);
        fclose(my_file);
    }
    else{
        printf("No message found\n");
    }

}


// ===================================================================  Free at command executer function
void free_command(int port, char command[]){

    // prepare(port);

    char recieved[50];
    int timeOut=100;
    int len=0;
    char now[50];

    sprintf(now, "%s\r", command);
    write_port(port, now, strlen(now));

    while(len<5 && timeOut){
        printf("%d --- %d\n", timeOut, len);
        len = read_port(port, recieved, sizeof(recieved));
        timeOut--;
    }

    printf("The result is: %s\n", recieved);;
}


// ===================================================================  Static report function
void static_report(int port){

    // prepare(port);

    char comm[255];
    char resp[150];
    char text[255];
    int timeOut = 10;
    int len = 0;
    int new_line = 0;
    int quote = 0;
    int limit = 0;

/// =============================== Operator checking
    memset(comm,0,strlen(comm));
    memset(resp,0,strlen(resp));
    memset(text,0,strlen(text));
    
    sprintf(comm, "AT+COPS?\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){

        if(quote == 1 && limit < 8){
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '"'){
            quote++;
        }

    }
    printf("\nRegistered operator is: %s\n", text);

/// ================================ Manufacturer Name
    memset(comm,0,strlen(comm));
    memset(resp,0,strlen(resp));
    memset(text,0,strlen(text));

    new_line = 0;
    limit = 0;
    sprintf(comm, "AT+GMI\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(new_line == 1 && limit < 8) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    printf("Manufacturer Name is: %s\n", text);
/// ================================= Brand Name
    memset(comm,0,strlen(comm));
    memset(resp,0,strlen(resp));
    memset(text,0,strlen(text));

    new_line = 0;
    limit = 0;
    sprintf(comm, "AT+GMM\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){

        if(new_line == 1 && limit < 4) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    printf("Brand name is: %s\n", text);
/// ================================== Version
    memset(comm,0,strlen(comm));
    memset(resp,0,strlen(resp));
    memset(text,0,strlen(text));

    new_line = 0;
    limit = 0;
    sprintf(comm, "AT+GMR\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(new_line == 1 && limit < 19) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    printf("Version is: %s\n", text);
/// ================================== IMEI Code
    memset(comm,0,strlen(comm));
    memset(resp,0,strlen(resp));
    memset(text,0,strlen(text));

    new_line = 0;
    limit = 0;
    sprintf(comm, "AT+GSN\r\n");
    write_port(port, comm, strlen(comm));
    read_port(port, resp, sizeof(resp));

    for(int j=0; j<strlen(resp); j++){
        if(new_line == 1 && limit < 16) {
            strncat(text, &resp[j], 1);
            limit++;
        }
        if(resp[j] == '\n'){
            new_line++;
        }
    }
    printf("IMEI Code is: %s\n\n", text);
}


// ============================================================  Persian SMS
int persian_sms(int port, int timeOut){
    char *letters[] = {" ", "ض", "ص", "ث", "ق", "ف", "غ", "ع", "ه", "خ", "ح", "ج", "ش", "س", "ی", "ب", "ل", "ا", "ت", "ن", "م", "ک", "ظ", "ط", "ز", "ر", "ذ", "د", "و", "گ", "چ", "پ", "ژ"};
    const char *indexes[] = {"0636", "0635", "062B", "0642", "0641", "063A", "0639", "0647", "062E", "062D", "062C", "0634", "0633", "06CC", "0628", "0644", "0627", "062A", "0646", "0645", "06A9", "0638", "0637", "0632", "0631", "0630", "062F", "0648", "06AF", "0686", "067E", "0698", "0020"};
    const int len = (sizeof(indexes) / sizeof(char *));
    char message[500];
    char nothing[10];
    char phone_number[20];
    int temp;
    char decodes[400];
    char char_name[3];
    char final_string[1000];

    memset(message, 0, strlen(message));
    memset(phone_number, 0, strlen(phone_number));
    memset(final_string, 0, strlen(final_string));
    memset(decodes, 0, strlen(decodes));

    strcat(final_string, "079189290000900231000B81");

    write_port(port, "AT+CMGF=0\r\n", strlen("AT+CMGF=0\r\n"));
    sleep(1);

    printf("Enter your message: ");
    fgets(message, sizeof(message), stdin);
    scanf("%[^\n]%*c", message);

    printf("Enter your phone number: ");
    scanf("%s", phone_number);
    strcat(phone_number, "F");


    for (int i=0; i<strlen(phone_number); i+=2){
        char_name[0] = phone_number[i+1];
        char_name[1] = phone_number[i];
        char_name[2] = '\0';
        strcat(final_string, char_name);
    }
    // printf("The phone number added: %s \n", final_string);
    strcat(final_string, "0008FF");


    int i=0;
    while (i<strlen(message)){
        if (message[i] == ' '){
            strcat(decodes, indexes[len-1]);
            i++;
        }
        else{
            char_name[0] = message[i];
            
            char_name[1] = message[i+1];
            char_name[2] = '\0';
            for (int j=0; j<len; j++){
                if(!strcmp(letters[j], char_name)){
                    strcat(decodes, indexes[j-1]);
                }
            }
            i+=2;
    }
    }

    // printf("This is decoded: %s \n", decodes);

    char size_of_num[5];
    char comm[700];
    temp = strlen(decodes) / 2;
    sprintf(size_of_num, "%x", temp);

    if (strlen(size_of_num) == 1){
        strcat(final_string, "0");
    }
    strcat(final_string, size_of_num);
    // printf("Before con: %s \n", final_string);
    strcat(final_string, decodes);
    // printf("After con: %s \n", final_string);


    // printf("The final string is: %s\n", final_string);

    temp = strlen(final_string) - 16;
    temp /= 2;
    // printf("The total length should be: %i \n", temp);

    sprintf(comm, "AT+CMGS=%i\r", temp);
    write_port(port, comm, strlen(comm));
    read_comm(port, ">", timeOut);

    write_port(port, final_string, strlen(final_string));
    sprintf(comm, "%c", (char)26);
    write_port(port, comm, strlen(comm));

    read_comm(port, "OK", timeOut);

    sleep(1);
    write_port(port, "AT+CMGF=1\r\n", strlen("AT+CMGF=1\r\n"));
    sleep(1);
}

// ================================================================== Check buffer for a space
int check_buffer()
{
    for (int i=0; i < BUFFER_SIZE; i++)
    {
        if (buffer[i] == NULL)
        {
            return i;
        }
    }
    return -1;
}

// ================================================================== Storing all messages in the buffer
int command_getter(char type[], char number[], char text[])
{
    int index = check_buffer();

    if (index == -1)
    {
        printf("Buffer is FULL\n");
        return -1;
    }
    command* me = malloc(sizeof(command));
    me -> type = type;
    me -> text = text;
    me -> number = number;
    buffer[index] = me;
    return 0;
}

// ================================================================= Execute all commands
void com_reader()
{
    char command_type[50];

    for (int i = 0; i < BUFFER_SIZE; i++)
    {
        if (buffer[i] != NULL)
        {
            sprintf(command_type, "%s", buffer[i]->type);
             
            if (!strcmp(command_type, "call"))
            {
                printf("Calling to %s number \n", buffer[i]->number);
                
            }
            else if (!strcmp(command_type, "send_sms"))
            {
                printf("Sending message to %s with the text of %s \n", buffer[i]->number, buffer[i]->text);
            }
            buffer[i] = NULL;
        }
    }
    free(command_type);
}

// ===================================================================  The program main function
int main()
    {
        printf("Hello from the first AMS program\n");
        const char * device = "/dev/AMS_EC25_AT";

        uint32_t baud_rate = 115200;
        int fd = init_modem(device, baud_rate);

        if (fd < 0) { return 1; }
            initial_commands(fd);
            static_report(fd);

            char command[20];
            char recieved[50];

            while(1){

                tcflush(fd, TCIOFLUSH);
                memset(recieved, 0, strlen(recieved));
                memset(command, 0, strlen(command));
                // read_sms(fd);
                printf("Enter a command to be executed: ");
                scanf("%s", &command);

                if (!strcmp(command, "stop")) {
                    break;
                }
                else if (!strcmp(command, "call")){
                    call_number(fd, 10);
                }
                else if(!strcmp(command, "dynamic")){
                    dynamic_report(fd);
                }
                else if(!strcmp(command, "static")){
                    static_report(fd);
                }
                else if(!strcmp(command, "sms")){
                    char text[255];
                    char number[50];

                    printf("Text: ");
                    fgets(text, sizeof(text), stdin);
                    scanf("%[^\n]%*c", text);

                    printf("Phone Number: ");
                    scanf("%s", number);

                    if (strlen(text) > 100){
                        printf("Multi page is called \n");
                        multi_page_sms(fd, text, number, 20);
                    }
                    else{
                        printf("single page is called \n");
                        single_page_sms(fd, text, number, 20);
                    }
                }
                else if(!strcmp(command, "per")){
                    persian_sms(fd, 50);
                }
                else if(!strcmp(command, "rd"))
                {
                    read_me(fd);
                }
                else{
                        free_command(fd, command);
                }
                tcflush(fd, TCIOFLUSH);
        }
        close(fd);
        return 0;
}


