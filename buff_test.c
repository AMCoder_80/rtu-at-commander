#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

typedef struct command
{
  char *type;
  char *number;
  char *text;
}command;


command* coms[30] = {NULL};
const int SIZE = (sizeof(coms)) / (sizeof(struct command));


int check_buffer()
{
    for (int i=0; i < SIZE; i++)
    {
        if (coms[i] == NULL)
        {
            return i;
        }
    }
    return -1;
}


int com_getter(char type[], char number[], char text[])
{
    int index = check_buffer();

    if (index == -1)
    {
        printf("Buffer is FULL\n");
        return -1;
    }
    command* me = malloc(sizeof(command));
    printf("Size is: %lu \n", sizeof(command));
    printf("This is the address: %p \n", me);
    me -> type = type;
    me -> text = text;
    me -> number = number;
    coms[index] = me;
    return 0;
}


void com_reader()
{
    char command_type[50];

     for (int i = 0; i < SIZE; i++)
     {
         if (coms[i] != NULL)
         {
             sprintf(command_type, "%s", coms[i]->type);
             
             if (!strcmp(command_type, "call"))
             {
                 printf("Calling to %s number \n", coms[i]->number);
             }
             else if (!strcmp(command_type, "send_sms"))
             {
                 printf("Sending message to %s with the text of %s \n", coms[i]->number, coms[i]->text);
             }
             
            coms[i] = NULL;
         }
     }
}


int main(void)
{
    com_getter("call", "09369947270", "");
    com_getter("send_sms", "09915647439", "Hello World!");
    com_getter("send_sms", "09915647439", "Goodbye World?");
    com_getter("static", "", "");
    com_getter("per", "09915647439", "سلام چطوری");
    com_getter("get_sms", "", "");
    com_reader();
    
    printf("---------->> Done for first iteration <<----------------\n");
    com_reader();

}

