#ifndef modem_h__free_command
#define modem_h__


typedef struct command
{
    int port;
    char *type;
    char *number;
    char *text;
}command;


extern int init_modem(const char * device, uint32_t baud_rate);
extern int write_port(int fd, char * buffer, size_t size);
extern ssize_t read_port(int fd, char * buffer, size_t size);
extern int read_comm(int port, char pattern[], int timeOut);
extern int delivered(int port, int timeOut);
extern ssize_t call_number(int port, char number[], int timeOut);
extern ssize_t single_page_sms(int port, char text[], char number[], int timeOut);
extern ssize_t multi_page_sms(int port, char text[], char number[], int timeOut);
extern void initial_commands(int port);
extern void dynamic_report(int port);
extern void read_sms(int port);
extern void founder(char* resp);
extern void static_report(int port);
extern int persian_sms(int port, char message[], char number[], int timeOut);
extern void send_sms(int port, char text[], char number[], int timeOut);
extern void* command_reader(void* arg);
int serial_flush(int port);
#endif  // modem_h__
